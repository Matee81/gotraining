package main

import (
	"errors"
	"fmt"
	"io/ioutil"
	"strings"
)

const MatchesPlayedindex = 0
const MatchesWonindex = 1
const MatchesDrawnindex = 2
const MatchesLostindex = 3
const Pointsindex = 4

var result = make(map[string][]int)

func main() {
	dat, err := ioutil.ReadFile("matches.txt")
	if err != nil {
		panic(err)
	}
	matchesList := strings.Split(strings.ToUpper(string(dat)), "\n")
	if  err :=processMatchesList(matchesList); err!=nil {
		fmt.Println(err)
		return
	}
	fmt.Println(result) //Display Result

}

func processMatchesList(matchesList []string) error{
	for _, match := range matchesList { //Processing each match
		matchData := strings.Split(match, ";")
		matchData[2] = strings.TrimRight(matchData[2], "\r\n")
		if err := validateMatchData(matchData); err != nil {
			return err
		}
		processMatchData(matchData)

	}
	return nil
}

func validateMatchData(matchData []string) error {
	if len(matchData) != 3 || strings.TrimSpace(matchData[0]) == "" || strings.TrimSpace(matchData[1]) == "" || strings.TrimSpace(matchData[2]) == "" {
		return errors.New("Error with Input match data: " + strings.Join(matchData, ";"))
	}
	res := matchData[2]
	if !func() bool {
		if res == "WIN" || res == "DRAW" || res == "LOSS" {
			return true
		}
		return false
	}() {
		return errors.New("Result Condition invalid")
	}

	return nil
}

func processMatchData(matchData []string) {
	if _, ok := resultMap[matchData[0]]; !ok {
		result[matchData[0]] = []int{0, 0, 0, 0, 0}
	}

	if _, ok := resultMap[matchData[1]]; !ok {
		result[matchData[1]] = []int{0, 0, 0, 0, 0}
	}
	processMatchResult(matchData)
}

func processMatchResult(matchData []string) {

	result[matchData[0]][MatchesPlayedindex]++
	result[matchData[1]][MatchesPlayedindex]++
	if matchData[2] == "WIN" {
		result[matchData[0]][MatchesWonindex]++
		result[matchData[1]][MatchesLostindex]++
		result[matchData[0]][Pointsindex] += 3
	} else if matchData[2] == "LOSS" {
		result[matchData[0]][MatchesLostindex]++
		result[matchData[1]][MatchesWonindex]++
		result[matchData[1]][Pointsindex] += 3
	} else {
		result[matchData[0]][MatchesDrawnindex]++
		result[matchData[1]][MatchesDrawnindex]++
		result[matchData[0]][Pointsindex]++
		result[matchData[1]][Pointsindex]++
	}
}
