package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

func main() {

	fmt.Println("Enter a phrase ")
	r := bufio.NewReader(os.Stdin)
	phrase, err := r.ReadString('\n')
	if err != nil {
		fmt.Println("Please type your message again! Error: ", err)
		return
	}
	words := strings.Split(phrase, " ")
	lastIdx := len(words) - 1
	words[lastIdx] = strings.TrimRight(words[lastIdx], "\r\n")

	wordsCount := getWordsCount(words)
	fmt.Println(wordsCount)
}

func getWordsCount(words []string) (wordsCount map[string]int) {
	wordsCount = make(map[string]int)
	for _, word := range words {
		word = strings.ToUpper(strings.TrimSpace(word))
		if _, ok := wordsCount[word]; !ok {
			wordsCount[word] = 1
		} else {
			wordsCount[word]++
		}
	}
	return wordsCount

}
