package main

import (
	"fmt"
)

func main() {

	var num int
	fmt.Println("Enter the value of N")
	_, err := fmt.Scanln(&num)
	if err != nil {
		fmt.Println("Error: ", err)
		return
	}

	fmt.Println("Primary number: ", nthPrimaryNumber(num))
}

func nthPrimaryNumber(n int) int {
	counter := 0

	for i := 1; ; i++ {
		if isPrime(i) {
			counter++
		}
		if counter == n {
			return i
		}
	}
}

func isPrime(num int) bool {
	n := num / 2
	for i := 2; i <= n; i++ {
		if num%i == 0 {
			return false
		}
	}
	return true
}
