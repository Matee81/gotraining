package main

import (
	"errors"
	"fmt"
	"unicode"
)

func main() {
	var strandA, strandB string

	fmt.Println("Enter configuration for DNA strand A:")
	_, err := fmt.Scanln(&strandA)
	if err != nil {
		fmt.Println("Error: ", err)
	}
	fmt.Println("Enter configuration for DNA strand B:")
	_, err = fmt.Scanln(&strandB)
	if err != nil {
		fmt.Println("Error: ", err)
	}

	if len(strandA) != len(strandB) {
		fmt.Println("Error: Both strings should be of equal length")
		return
	}

	configDiff, err := getConfigDifference(strandA, strandB)
	if err != nil {
		fmt.Println("Error: ", err)
		return
	}

	fmt.Println("Difference: ", configDiff)

}

func getConfigDifference(strandA, strandB string) (int, error) {
	diffCount := 0
	for i := 0; i < len(strandA); i++ {
		stA := rune(strandA[i])
		stB := rune(strandB[i])
		if !unicode.IsLetter(stA) || !unicode.IsLetter(stB) {
			return -1, errors.New("DNA configuration can only have a letter in it")
		}
		if unicode.ToUpper(stA) != unicode.ToUpper(stB) {
			diffCount++
		}
	}

	return diffCount, nil
}
