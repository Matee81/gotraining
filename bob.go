package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

func main() {
	fmt.Println("Welcome to chatbot bob. Press E anytime to end conversation.")
	for {
		fmt.Println("You: ")
		r := bufio.NewReader(os.Stdin)
		input, err := r.ReadString('\n')
		if err != nil {
			fmt.Println("Please type your message again! Error: ", err)
		} else {
			endConvo := generateReply(input)
			if endConvo {
				break
			}
		}
	}
}

func generateReply(input string) (endConvo bool) {
	input = input[:len(input)-2]
	isQuestion, isYelling, isNothing := understandMessage(input)

	if len(input) > 0 && strings.ToUpper(input) == "E" {
		fmt.Println("Bob: Bye!")
		return true
	} else if isNothing || len(input) == 0 {
		fmt.Println("Bob: Whoa, Fine. Be that way!")
	} else if isQuestion && isYelling {
		fmt.Println("Bob: Calm down, I know what I'm doing!")
	} else if isQuestion {
		fmt.Println("Bob: Sure.")
	} else if isYelling {
		fmt.Println("Bob: Whoa, chill out!")
	} else {
		fmt.Println("Bob: Whatever!")
	}
	return false
}

func understandMessage(input string) (isQuestion bool, isYelling bool, isNothing bool) {
	isQuestion = false
	isNothing = true
	isYelling = true
	if len(input) > 0 && input[len(input)-1] == '?' {
		isQuestion = true
	}
	for i := 0; i < len(input); i++ {
		if input[i] >= 97 && input[i] <= 122 {
			isYelling = false
		}
		if input[i] != ' ' {
			isNothing = false
		}
	}
	return isQuestion, isYelling, isNothing

}
