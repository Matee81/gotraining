package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

func main() {
	fmt.Println("Welcome to Acronym Generator.\nInput:")
	r := bufio.NewReader(os.Stdin)
	input, err := r.ReadString('\n')
	if err != nil {
		fmt.Println("Error: ", err)
	} else {
		input = input[:len(input)-2]
		if len(input) == 0 {
			fmt.Println("Cannot process Empty string.")
			return
		}
		fmt.Println(generateAcronym(input))
	}
}
func generateAcronym(input string) (acronym string) {
	wordsList := strings.Split(strings.TrimSpace(input), " ")

	for i := 0; i < len(wordsList); i++ {
		acronym = acronym + strings.ToUpper(string(wordsList[i][0]))
	}
	return acronym
}