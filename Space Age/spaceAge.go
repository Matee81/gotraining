package main

import (
	"errors"
	"fmt"
	"strings"
)

type planet string

func main() {
	var seconds float64
	var planetName planet
	fmt.Println("Enter Planet Name: ")
	_, err := fmt.Scanln(&planetName)
	if err != nil {
		fmt.Println("Error: ", err)
		return
	}

	fmt.Println("Enter age in seconds: ")
	_, err = fmt.Scanln(&seconds)
	if err != nil {
		fmt.Println("Error: ", err)
		return
	}

	age, err := getAge(planetName, seconds)
	if err != nil {
		fmt.Println("Error: ", err)
		return
	}

	fmt.Println(age, " "+planetName+" years old")

}

func getAge(planetName planet, seconds float64) (age float64, err error) {
	switch strings.ToUpper(string(planetName)) {
	case "EARTH":
		return seconds / (365.25 * 24 * 3600), nil
	case "MERCURY":
		return seconds / (0.2408467 * 365.25 * 24 * 3600), nil
	case "VENUS":
		return seconds / (0.61519726 * 365.25 * 24 * 3600), nil
	case "SATURN":
		return seconds / (29.447498 * 365.25 * 24 * 3600), nil
	case "JUPITER":
		return seconds / (11.862615 * 365.25 * 24 * 3600), nil
	case "MARS":
		return seconds / (1.8808158 * 365.25 * 24 * 3600), nil
	case "URANUS":
		return seconds / (84.016846 * 365.25 * 24 * 3600), nil
	case "NEPTUNE":
		return seconds / (164.79132 * 365.25 * 24 * 3600), nil
	default:
		return 0.00, errors.New("Invalid Planet Name")
	}

}
